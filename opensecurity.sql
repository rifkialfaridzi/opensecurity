-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 25, 2022 at 03:45 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `opensecurity`
--

-- --------------------------------------------------------

--
-- Table structure for table `aktivitas`
--

CREATE TABLE `aktivitas` (
  `id` int(11) NOT NULL,
  `record_absen` int(11) NOT NULL,
  `content` text NOT NULL,
  `foto` varchar(100) NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `aktivitas`
--

INSERT INTO `aktivitas` (`id`, `record_absen`, `content`, `foto`, `tanggal`) VALUES
(1, 24, 'Memulai Shift Pagi', 'dummy.jpg', '2022-06-03 01:45:40'),
(2, 24, 'Ada Sesuatu di belik tembok', 'dummy.jpg', '2022-06-03 01:46:21'),
(4, 24, 'Ada Orang Mencurigakan Mas', 'IwPTbX703d.jpg', '2022-06-03 02:15:32'),
(5, 24, 'Selamat Hari Kelahiran Pancasila', 'dmcthSGRBg.jpg', '2022-06-03 20:04:47'),
(6, 26, 'Madang Sik Lur', 'vJHwCu4cF2.jpg', '2022-06-05 09:34:22'),
(7, 27, 'Memulai Shift', 'dummy.jpg', '2022-06-12 20:23:58'),
(8, 27, 'Keadaaan Aman Terkendali', 'IrdYF3k9y5.jpg', '2022-06-12 20:24:13'),
(9, 29, 'cek lapangan', '4OrCz9vJUS.jpg', '2022-06-24 16:52:04'),
(10, 33, 'Cek Kondisi Pegawai', '3dGnROHstA.png', '2022-06-28 10:29:41'),
(11, 34, 'Memulai absen', 'dummy.jpg', '2022-07-08 19:21:01'),
(12, 40, 'Kondisi aman', 'UHMpnJgKui.png', '2022-07-15 08:59:34'),
(13, 41, 'Aman Terkendali', 'jA8HfPD2OS.png', '2022-07-15 09:07:01');

-- --------------------------------------------------------

--
-- Table structure for table `gaji`
--

CREATE TABLE `gaji` (
  `id` int(11) NOT NULL,
  `periode` date NOT NULL,
  `jumlah_hari` int(11) NOT NULL,
  `lembur` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gaji`
--

INSERT INTO `gaji` (`id`, `periode`, `jumlah_hari`, `lembur`) VALUES
(3, '2022-05-01', 24, 25000),
(4, '2022-06-01', 25, 15000);

-- --------------------------------------------------------

--
-- Table structure for table `ijin`
--

CREATE TABLE `ijin` (
  `id` int(11) NOT NULL,
  `pegawai` int(11) NOT NULL,
  `jumlah_hari` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `jenis` enum('ijin','cuti') NOT NULL,
  `tanggal` date NOT NULL,
  `status` enum('diterima','ditolak','diproses') NOT NULL,
  `status_keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ijin`
--

INSERT INTO `ijin` (`id`, `pegawai`, `jumlah_hari`, `keterangan`, `jenis`, `tanggal`, `status`, `status_keterangan`) VALUES
(3, 45, 4, 'Sakit', 'cuti', '2022-05-23', 'diterima', ''),
(4, 45, 3, 'Sakit', 'ijin', '2022-05-24', 'diterima', ''),
(5, 45, 3, 'Sakit', 'ijin', '2022-05-23', 'ditolak', ''),
(6, 45, 3, 'Sakit', 'cuti', '2022-05-22', 'diproses', ''),
(7, 45, 2, 'Sakit', 'cuti', '2022-05-24', 'diterima', ''),
(8, 52, 1, 'Sakit', 'ijin', '2022-06-28', 'diterima', ''),
(9, 49, 1, 'Sakit', 'ijin', '2022-06-28', 'diproses', ''),
(10, 55, 1, 'Harus istirahat', 'ijin', '2022-07-10', 'ditolak', 'Terlalu Lama'),
(11, 50, 2, 'Sakit', 'cuti', '2022-07-25', 'diproses', '');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id`, `nama`) VALUES
(1, 'Gada Pratama'),
(2, 'Gada Madya'),
(3, 'Gada Utama'),
(7, 'Komandan Regu'),
(8, 'Manajer lapangan'),
(12, 'Manager');

-- --------------------------------------------------------

--
-- Table structure for table `lokasi`
--

CREATE TABLE `lokasi` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `masa_kontrak` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lokasi`
--

INSERT INTO `lokasi` (`id`, `nama`, `alamat`, `masa_kontrak`) VALUES
(2, 'PT Sritex Global', 'Sukoharjo Jawa Tengah', '2027-06-01'),
(3, 'PT Sari Warna Solo', 'Laweyan Surakarta', '2023-06-01'),
(4, 'PT Sri Rejeki', 'Sukoharjo', '2024-07-10');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `user` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `nik` varchar(50) NOT NULL,
  `nomor_telepon` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `jabatan` int(11) NOT NULL,
  `lokasi` int(11) NOT NULL,
  `shift` int(11) NOT NULL,
  `tanggal_masuk` date DEFAULT NULL,
  `gaji_pokok` varchar(50) NOT NULL,
  `tunjangan` varchar(50) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`user`, `nama`, `tanggal_lahir`, `nik`, `nomor_telepon`, `alamat`, `jabatan`, `lokasi`, `shift`, `tanggal_masuk`, `gaji_pokok`, `tunjangan`, `foto`) VALUES
(49, 'Bambang Kusumo', '2022-06-01', '23.42.34.23.42.34-2342', '6285 1562 3006', '<p>Grogol Sukoharjo</p>', 3, 2, 10, '2022-06-01', '', '', '3KUWkhQ4Yw.jpg'),
(50, 'Rikardo Milos', '2022-06-02', '32.42.34.32.42.34-2345', '4563 5245 2525', '<p>Solo</p>', 3, 3, 11, '2022-06-02', '', '', 'x3uOZsRDv1.png'),
(55, 'Adjie Nugroho', '1999-03-14', '12.23.58.80.88.45-7579', '0858 5357 8899', '<p>Sukoharjo</p>', 8, 2, 10, '2022-07-10', '', '', 'dummy.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `record_absen`
--

CREATE TABLE `record_absen` (
  `id` int(11) NOT NULL,
  `shift` int(11) NOT NULL,
  `pegawai` int(11) NOT NULL,
  `waktu` time DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `gap` float NOT NULL,
  `jenis` enum('absen masuk','absen pulang','lembur masuk','lembur pulang','cuti') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `record_absen`
--

INSERT INTO `record_absen` (`id`, `shift`, `pegawai`, `waktu`, `tanggal`, `gap`, `jenis`) VALUES
(5, 10, 45, '07:21:00', '2022-05-17', 1, 'absen masuk'),
(8, 10, 45, '07:21:00', '2022-05-18', 0, 'absen masuk'),
(9, 10, 45, '20:51:00', '2022-05-19', 14, 'absen masuk'),
(11, 10, 45, '20:55:00', '2022-05-19', 0, 'absen pulang'),
(12, 10, 45, '10:02:00', '2022-05-22', 4, 'absen masuk'),
(16, 10, 45, '10:41:00', '2022-05-22', 0, 'absen pulang'),
(17, 10, 45, '08:42:00', '2022-05-22', 0, 'lembur masuk'),
(21, 10, 45, '11:01:00', '2022-05-22', 2, 'lembur pulang'),
(22, 10, 45, '11:01:00', '2022-05-22', 3, 'lembur pulang'),
(23, 10, 45, '20:06:00', '2022-06-02', 14, 'absen masuk'),
(24, 10, 45, '06:17:00', '2022-06-03', 0, 'absen masuk'),
(25, 10, 45, '19:57:00', '2022-06-03', 0, 'absen pulang'),
(26, 10, 45, '09:33:00', '2022-06-05', 3, 'absen masuk'),
(27, 10, 45, '20:23:00', '2022-06-12', 14, 'absen masuk'),
(28, 10, 49, '14:54:00', '2022-06-23', 8, 'absen masuk'),
(29, 10, 49, '16:51:00', '2022-06-24', 10, 'absen masuk'),
(30, 10, 49, '16:52:00', '2022-06-24', 0, 'absen pulang'),
(31, 11, 52, '18:40:00', '2022-06-24', 5, 'absen masuk'),
(32, 11, 52, '10:03:00', '2022-06-28', -3, 'absen masuk'),
(33, 10, 49, '10:04:00', '2022-06-28', 4, 'absen masuk'),
(34, 10, 45, '19:20:00', '2022-07-08', 13, 'absen masuk'),
(35, 10, 55, '20:20:00', '2022-07-10', 14, 'absen masuk'),
(36, 10, 55, '20:22:00', '2022-07-10', 0, 'absen pulang'),
(37, 10, 55, '17:00:00', '2022-07-11', 11, 'absen masuk'),
(38, 10, 55, '17:07:00', '2022-07-11', 0, 'absen pulang'),
(39, 10, 49, '10:18:00', '2022-07-13', 4, 'absen masuk'),
(40, 10, 55, '08:57:00', '2022-07-15', 2, 'absen masuk'),
(41, 11, 50, '09:05:00', '2022-07-15', -4, 'absen masuk');

-- --------------------------------------------------------

--
-- Table structure for table `shift`
--

CREATE TABLE `shift` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `jam_masuk` time NOT NULL,
  `jam_pulang` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shift`
--

INSERT INTO `shift` (`id`, `nama`, `jam_masuk`, `jam_pulang`) VALUES
(10, 'Shift Pagi', '07:00:00', '16:00:00'),
(11, 'Shift Siang', '14:00:00', '23:00:00'),
(13, 'Shift Malam', '22:00:00', '07:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`, `created_at`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, '2020-03-21'),
(49, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 3, '2022-06-01'),
(50, 'iks', '21232f297a57a5a743894a0e4a801fc3', 3, '2022-06-02'),
(51, 'mandor', '21232f297a57a5a743894a0e4a801fc3', 2, '2020-03-21'),
(55, '273', '4734ba6f3de83d861c3176a6273cac6d', 3, '2022-07-10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aktivitas`
--
ALTER TABLE `aktivitas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `record_absen` (`record_absen`);

--
-- Indexes for table `gaji`
--
ALTER TABLE `gaji`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ijin`
--
ALTER TABLE `ijin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pegawai` (`pegawai`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD UNIQUE KEY `user` (`user`) USING BTREE,
  ADD KEY `jabatan` (`jabatan`),
  ADD KEY `lokasi` (`lokasi`),
  ADD KEY `shift` (`shift`);

--
-- Indexes for table `record_absen`
--
ALTER TABLE `record_absen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pegawai` (`pegawai`),
  ADD KEY `shift` (`shift`) USING BTREE;

--
-- Indexes for table `shift`
--
ALTER TABLE `shift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aktivitas`
--
ALTER TABLE `aktivitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `gaji`
--
ALTER TABLE `gaji`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ijin`
--
ALTER TABLE `ijin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `lokasi`
--
ALTER TABLE `lokasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `record_absen`
--
ALTER TABLE `record_absen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `shift`
--
ALTER TABLE `shift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
