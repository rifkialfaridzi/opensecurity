<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Aktivitas extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$data_session = $this->session->userdata;

		$this->load->model('Aktivitas_model');
		$this->load->model('Pegawai_model');
		$this->load->model('Profile_model');
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function index()
	{
		$data_session = $this->session->userdata;
		$validasi_data = $this->Profile_model->record_absen_validation(["pegawai" => $data_session['id'], "tanggal" => date('Y-m-d'), "jenis" => "absen masuk"]);
		$data_aktivitas = $this->Aktivitas_model->get_aktivitas_by_pegawai($data_session['id']);

		$data['main_content'] = 'profile/aktivitas/aktivitas';
		$data['page_title'] = 'Selamat Datang Silahkan Melakukan Aktivitas';
		$data['cek_absen'] = $validasi_data;
		$data['shift'] = $this->Pegawai_model->get_by_id($data_session['id'], "absen masuk");
		$data['data_aktivitas'] = $data_aktivitas;
		$data['absen_masuk'] = $this->Profile_model->record_absen($data_session['id'], "absen masuk");
		$this->load->view('template', $data);
	}

	public function detail($record_absen = null, $pegawai = null)
	{
		if (is_null($record_absen) && is_null($pegawai)) {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('admin'));
		} else {
			$data_aktivitas = $this->Aktivitas_model->get_aktivitas($record_absen, $pegawai);

			if (empty($data_aktivitas)) {
				$this->session->set_flashdata('pesan', 'Tidak Ada Aktivitas Apapun');
				redirect(site_url('pegawai'));
			} else {

				$data['main_content'] = 'profile/aktivitas/detail';
				$data['page_title'] = 'Selamat Datang Silahkan Melakukan Aktivitas';

				$data['shift'] = $this->Pegawai_model->get_by_id($pegawai);
				$data['data_aktivitas'] = $data_aktivitas;
				$this->load->view('template', $data);
			}
		}
	}

	public function create()
	{

		$data['main_content'] = 'jabatan/create';
		$data['page_title'] = 'Halaman Jabatan';
		$this->load->view('template', $data);
	}


	public function json()
	{
		header('Content-Type: application/json');
		$Jabatan =  $this->Jabatan_model->json();

		$data['draw'] = 0;
		$data['recordsTotal'] = $Jabatan == null ? [] : count($Jabatan);
		$data['recordsFiltered'] = $Jabatan == null ? [] : count($Jabatan);
		$data['data'] = $Jabatan == null ? [] : $Jabatan;

		echo json_encode($data);
	}


	public function create_action()
	{
		//var_dump($this->input->post());
		$this->_rules_create();
		date_default_timezone_set('Asia/Jakarta');
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Disimpan </br>' . validation_errors());
			redirect(site_url('aktivitas'));
			//echo validation_errors();
		} else {
			$data_session = $this->session->userdata;
			$validasi_data = $this->Profile_model->record_absen_validation(["pegawai" => $data_session['id'], "tanggal" => date('Y-m-d'), "jenis" => "absen masuk"]);

			$data_post = $this->input->post();

			if (isset($_FILES['foto']['name']) && !empty($_FILES['foto']['name'])) {

				// DO UPLOAD
				$file_name = str_replace('.', '', random_string('alnum', 10));
				$config['upload_path'] = './assets/uploads/';
				$config['allowed_types']        = 'gif|jpg|jpeg|png';
				$config['file_name']            = $file_name;
				$config['overwrite']            = true;
				$config['max_size']             = 1024; // 1MB
				$config['max_width']            = 1080;
				$config['max_height']           = 1080;

				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('foto')) {
					$this->session->set_flashdata('pesan', 'Data Gagal Disimpan </br>' . $this->upload->display_errors());
					redirect(site_url('pegawai'));
				} else {
					$uploaded_data = $this->upload->data();
					$data_post['foto'] = $uploaded_data['file_name'];
				}
			} else {
				$data_post['foto'] = "dummy.jpg";
			}

			$data_post['record_absen'] = $validasi_data->id;
			$data_post['tanggal'] = date("Y-m-d H:i:s");


			// Insert Data Default Teknik Jabatan
			$this->Aktivitas_model->insert($data_post);
			$this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
			redirect(site_url('aktivitas'));
		}
	}

	public function edit($id)
	{

		$row = $this->Aktivitas_model->get_by_id($id);

		if ($row) {
			$data_session = $this->session->userdata;
			$validasi_data = $this->Profile_model->record_absen_validation(["pegawai" => $data_session['id'], "tanggal" => date('Y-m-d'), "jenis" => "absen masuk"]);
			$data_aktivitas = $this->Aktivitas_model->get_aktivitas_by_pegawai($data_session['id']);

			$data['main_content'] = 'profile/aktivitas/update';
			$data['page_title'] = 'Update Aktivitas';
			$data['aktivitas'] = $row;
			$data['cek_absen'] = $validasi_data;
			$data['shift'] = $this->Pegawai_model->get_by_id($data_session['id'], "absen masuk");
			$data['data_aktivitas'] = $data_aktivitas;
			$data['absen_masuk'] = $this->Profile_model->record_absen($data_session['id'], "absen masuk");
			$this->load->view('template', $data);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
			redirect(site_url('aktivitas'));
		}
	}

	public function update_action($id)
	{

		$row = $this->Aktivitas_model->get_by_id($id);

		$this->_rules_create();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Diubah </br>' . validation_errors());
			redirect(site_url('aktivitas'));
		} else {

			if (empty($row)) {
				$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
				redirect(site_url('aktivitas'));
			}

			$data_post = $this->input->post();

			if (isset($_FILES['foto']['name']) && !empty($_FILES['foto']['name'])) {

				// DO UPLOAD
				$file_name = str_replace('.', '', random_string('alnum', 10));
				$config['upload_path'] = './assets/uploads/';
				$config['allowed_types']        = 'gif|jpg|jpeg|png';
				$config['file_name']            = $file_name;
				$config['overwrite']            = true;
				$config['max_size']             = 1024; // 1MB
				$config['max_width']            = 1080;
				$config['max_height']           = 1080;

				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('foto')) {
					$this->session->set_flashdata('pesan', 'Data Gagal Disimpan </br>' . $this->upload->display_errors());
					redirect(site_url('pegawai'));
				} else {
					$uploaded_data = $this->upload->data();
					$data_post['foto'] = $uploaded_data['file_name'];
				}
			} else {
				$data_post['foto'] = $row->foto;
			}


			$this->Aktivitas_model->update($id, $data_post);
			$this->session->set_flashdata('pesan', 'Data Sukses Di Ubah');
			redirect(site_url('aktivitas'));
		}
	}

	public function delete($id)
	{
		$row = $this->Aktivitas_model->get_by_id($id);

		if ($row) {
			$this->Aktivitas_model->delete($id);
			$this->session->set_flashdata('pesan', 'Data Berhasil Di Hapus');
			redirect(site_url('aktivitas'));
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('aktivitas'));
		}
	}

	public function _rules_create()
	{
		$this->form_validation->set_rules('content', 'Keterangan', 'required');
		//$this->form_validation->set_rules('foto', 'foto', 'required');

		$this->form_validation->set_error_delimiters('<span class="text-white">', '</span>');
	}
}

/* End of file Category.php */
/* Location: ./application/controllers/Category.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */
