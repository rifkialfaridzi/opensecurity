<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $data_session = $this->session->userdata;

        if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 2) {
            redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
        }

     
        $this->load->model('Pegawai_model');
        $this->load->model('Profile_model');


        $this->load->model('auth_model');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function index()
    {

        $data_absen = $this->Pegawai_model->record_absen_user_all();

		$jumlah_ijin = 0;
		$data_ijin = $this->Pegawai_model->record_ijin_user_all();
		foreach ($data_ijin as $key) {
			$jumlah_ijin += intval($key->jumlah_hari);
		}

		$jumlah_absen_tepat_waktu = 0;
		$jumlah_absen_terlambat = 0;
		foreach ($data_absen as $key) {
			
			if (intval($key->gap) > 0) {
				$jumlah_absen_terlambat++;
			}else{
				$jumlah_absen_tepat_waktu++;
			}
		}

        $data['jumlah_absen_terlambat'] = $jumlah_absen_terlambat;
		$data['jumlah_absen_tepat_waktu'] = $jumlah_absen_tepat_waktu;
		$data['jumlah_ijin'] = $jumlah_ijin;

        $data['pegawai'] = count($this->Pegawai_model->get_all_Pegawai());
        $data['recent_absen'] = $this->Profile_model->all_record_absen_by_date();

        $data['main_content'] = 'admin/main';
        $data['page_title'] = 'Halaman Admin';
        $this->load->view('template', $data);
    }

    public function cek(){

        $totalPendapatan = $this->Penjualan_model->pendapatan_order_bymonth(6);
        var_dump($totalPendapatan);
    }

    public function dashboard($month)
    {

        $totalOrder =  count($this->Penjualan_model->total_order_bymonth($month));
        $totalProdukOrder = $this->Penjualan_model->jumlah_produk_order_bymonth($month)->total_produk;
        $totalPendapatan = $this->Penjualan_model->pendapatan_order_bymonth($month)->total_penjualan;
        $topProduk = $this->Penjualan_model->top_product_bymonth($month);
        $produk = $this->Barang_model->total_produk_bymonth($month);
        $supplier = $this->Supplier_model->total_supplier_bymonth($month);

        $data = [
            'totalOrder' => $totalOrder == null ? 0 : $totalOrder,
            'totalProdukOrder' => $totalProdukOrder == null ? 0 : $totalProdukOrder,
            'totalPendapatan' => $totalPendapatan == null ? 0 : $totalPendapatan,
            'topProduk' => $topProduk == null ? [] : $topProduk,
            'produk' => $produk,
            'supplier' => $supplier == null ? 0 : count($supplier),
        ];

        echo json_encode($data);
    }
}
