<section ng-app="myApp" ng-controller="someController" class="section">
  <div class="section-header">
    <h1>Dashboard</h1>
  </div>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          Halo Selamat Datang
        </div>
        <div class="card-body">
          <div class="row mt-4">
            <div class="col-12 col-lg-8 offset-lg-2">
              <div class="wizard-steps">

              <a href="<?php echo base_url('profile/absen/masuk'); ?>">
                <div class="wizard-step wizard-step-active">
                  <div class="wizard-step-icon">
                    <i class="fas fa-sign-in-alt"></i>
                  </div>
                  <div class="wizard-step-label">
                  &nbsp;&nbsp;&nbsp; Absen Masuk  &nbsp;&nbsp;&nbsp;
                  </div>
                </div>
                </a>

                <a href="<?php echo base_url('profile/absen/pulang'); ?>">
                <div class="wizard-step wizard-step-active">
                  <div class="wizard-step-icon">
                    <i class="fas fa-sign-out-alt"></i>
                  </div>
                  <div class="wizard-step-label">
                  &nbsp; Absen Pulang &nbsp;
                  </div>
                </div>
                </a>
                <a href="<?php echo base_url('ijin'); ?>">
                <div class="wizard-step wizard-step-active">
                  <div class="wizard-step-icon">
                    <i class="fas fa-user-tag"></i>
                  </div>
                  <div class="wizard-step-label">
                  &nbsp; Pengajuan Cuti &nbsp;
                  </div>
                </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<h4>Statistik Akumulasi Seluruh Data</h4>
					</div>
					<div class="card-body">
						<canvas id="myChart" height="158"></canvas>
					</div>
				</div>
			</div>
		</div>
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4>Absen Terkini</h4>
        </div>
        <div class="card-body p-0">
          <div class="table-responsive table-invoice">
            <table class="table table-striped">
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Shift</th>
                <th>Tanggal</th>
                <th>Waktu</th>
                <th>Status</th>
              </tr>
             
              <?php $i = 1;
              foreach ($recent_absen as $key) {

              ?>
                <tr>
                  <td> <?php echo $i; ?></td>
                  <td> <?php echo $key->nama_pegawai; ?></td>
                  <td> <?php echo $key->nama_shift; ?></td>
                  <td> <?php echo $key->tanggal; ?></td>
                  <td> <?php echo $key->waktu; ?></td>
                  <td> <?php echo $key->gap > 0 ? "Terlambat" : "Tepat Waktu"; ?></td>
                </tr>

              <?php if (++$i > 8) break;
              } ?>

            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script>

var ctx = document.getElementById("myChart").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'doughnut',
		data: {
			labels: [
				'Tepat Waktu',
				'Ijin',
				'Terlambat'
			],
			datasets: [{
				label: 'My First Dataset',
				data: [<?php echo $jumlah_absen_tepat_waktu; ?>, <?php echo $jumlah_absen_terlambat; ?>, <?php echo $jumlah_ijin; ?>],
				backgroundColor: [
					'rgb(255, 99, 132)',
					'rgb(54, 162, 235)',
					'rgb(255, 205, 86)'
				],
				hoverOffset: 4
			}]
		},
	});
  
  var app = angular.module('myApp', ['ui.bootstrap']);

  app.controller('someController', function($scope, $filter, $http) {



    $scope.selectMonth = function(month) {
      $http.get('<?php echo base_url("admin/dashboard/"); ?>' + month, {
        msg: 'hello word!'
      }).
      then(function(response) {
        $scope.totalSupplier = response.data.supplier;
        $scope.totalOrder = response.data.totalOrder;
        $scope.totalProdukOrder = response.data.totalProdukOrder;
        $scope.totalPendapatan = response.data.totalPendapatan;
        $scope.topProduk = response.data.topProduk;
        $scope.totalProduk = response.data.produk.jumlah_produk;

        console.log(response.data);
      }, function(response) {
        console.log('error bos');
      });

    }

    var date = new Date();
    $scope.selectMonth(date.getMonth() + 1);





  });
</script>