<section class="section">
	<div class="section-header">
		<div class="section-header-back">
			<button onclick="history.back()" class="btn btn-icon"><i class="fas fa-arrow-left"></i></button>
		</div>
		<h1>Absen Masuk : <?php echo $shift[0]->nama_shift; ?> - <?php echo $shift[0]->jam_masuk . " s/d " . $shift[0]->jam_pulang; ?></h1>
	</div>

	<div class="section-body">
		<?php if ($this->session->flashdata('pesan')) { ?>
			<div class="alert alert-warning alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					<?php echo $this->session->flashdata('pesan'); ?>
				</div>
			</div>
		<?php } ?>
		<?php if (!is_null($cek_absen)) { ?>
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h4>Update Aktivitas</h4>
						</div>
						<div class="card-body">
							<form method="POST" action="<?php echo base_url("aktivitas/update_action/").$aktivitas->id; ?>" enctype="multipart/form-data" class="needs-validation" novalidate="">
								<div class="form-group row mb-4">
									<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Foto</label>
									<div class="col-sm-12 col-md-7">
										<div id="image-preview" class="image-preview">
											<label for="image-upload" id="image-label">Pilih Gambar</label>
											<input type="file" name="foto" id="image-upload" accept="image/png, image/jpeg, image/jpg, image/gif" />
										</div>
										<div class="invalid-feedback">
											Foto Masih Kosong
										</div>
									</div>
								</div>
								<div class="form-group row mb-4">
									<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Keterangan</label>
									<div class="col-sm-12 col-md-7">
										<input type="text" class="form-control" value="<?php echo $aktivitas->content; ?>" name="content" tabindex="1" placeholder="Memulai Shift Pagi" required autofocus>
										<div class="invalid-feedback">
											Keterangan Masih Kosong
										</div>
									</div>
								</div>
								<div class="form-group row mb-4">
									<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
									<div class="col-sm-12 col-md-7">
										<button class="btn btn-primary">Tambahkan</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>

		<div class="row">
			<div class="col-12">
				<div class="activities">
					<?php foreach ($data_aktivitas as $key) { ?>
					<div class="activity">
						<div class="activity-icon bg-primary text-white shadow-primary">
							<i class="fas fa-comment-alt"></i>
						</div>
						<div class="activity-detail">
							<div class="mb-2">
								<span class="text-job text-primary"><?php echo $key->tanggal; ?></span>
								<div class="float-right dropdown">
									<a href="#" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
									<div class="dropdown-menu">
										<div class="dropdown-title">Options</div>
										<a href="<?php echo base_url("aktivitas/edit/").$aktivitas->id; ?>" class="dropdown-item has-icon"><i class="fas fa-list"></i> Ubah</a>
										<div class="dropdown-divider"></div>
										<a href="<?php echo base_url("aktivitas/delete/").$aktivitas->id; ?>" class="dropdown-item has-icon text-danger" data-confirm="Wait, wait, wait...|This action can't be undone. Want to take risks?" data-confirm-text-yes="Yes, IDC"><i class="fas fa-trash-alt"></i> Hapus</a>
									</div>
								</div>
							</div>
							<?php if ($key->foto != "dummy.jpg") { ?>
								<img style="width: 300px;height:300px" src="<?php echo base_url("assets/uploads/").$key->foto; ?>" alt="">
							<?php } ?>
							<p><?php echo $key->content; ?></p>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ADDONS LIBRARY -->
<script src="<?php echo base_url('assets/modules/cleave-js/dist/cleave.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/modules/cleave-js/dist/addons/cleave-phone.au.js'); ?>"></script>



<script>
	const d = new Date();
	let months = d.getMonth() + 1;
	let month = d.getMonth() + 1;
	console.log(month + 1);
	conter = 1;
	table = $('#unit_tabel').DataTable({
		// Load data for the table's content from an Ajax source
		"ajax": {
			"url": '<?php echo site_url('profile/data_absen/masuk/'); ?>' + months,
			"type": "POST"
		},
		"bFilter": false,
		//Set column definition initialisation properties.
		"columns": [{
				"data": null,
				"render": function(data, type, row) {
					return conter++;
				}
			},
			{
				"data": "nama_shift"
			},
			{
				"data": "tanggal"
			},
			{
				"data": "waktu"
			},
			{
				"data": null,
				"render": function(data, type, row) {
					return row.gap > 0 ? "Terlambat" : "Tepat Waktu";
				}
			},
		],

	});

	$('#month').on('change', function() {


		table.ajax.url("<?php echo site_url('profile/data_absen/masuk/'); ?>" + this.value).load();


	});
	$.uploadPreview({
		input_field: "#image-upload", // Default: .image-upload
		preview_box: "#image-preview", // Default: .image-preview
		label_field: "#image-label", // Default: .image-label
		label_default: "Choose File", // Default: Choose File
		label_selected: "Change File", // Default: Change File
		no_label: false, // Default: false
		success_callback: null // Default: null
	});
	$("#image-preview").attr("style", "background-image:url('<?php echo site_url("assets/uploads/") . $aktivitas->foto; ?>'); background-size:cover; background-position:center center");
</script>