<section class="section">
	<div class="section-header">
		<div class="section-header-back">
			<button onclick="history.back()" class="btn btn-icon"><i class="fas fa-arrow-left"></i></button>
		</div>
		<h1>Lembur Masuk</h1>
	</div>

	<div class="section-body">
		<?php if ($this->session->flashdata('pesan')) { ?>
			<div class="alert alert-warning alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					<?php echo $this->session->flashdata('pesan'); ?>
				</div>
			</div>
		<?php } else { ?>
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h4>Scan Lembur Masuk</h4>
						</div>
						<div class="card-body">

							<div style="width: 100%" id="reader"></div>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4>Riwayat Absensi</h4>
						<div class="card-header-action">
							<form>
								<div class="input-group">
									<select id="month" name="month" class="form-control selectric">
										<option value="1">Januari &nbsp;&nbsp;&nbsp;</option>
										<option value="2">Februari</option>
										<option value="3">Maret</option>
										<option value="4">April</option>
										<option value="5">Mei</option>
										<option value="6">Juni</option>
										<option value="7">Juli</option>
										<option value="8">Agustus</option>
										<option value="9">September</option>
										<option value="10">Oktober</option>
										<option value="11">November</option>
										<option value="12">Desember</option>
									</select>
								</div>
							</form>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="unit_tabel" class="table table-striped">
								<thead>
									<tr>
										<th>No</th>
										<th>Shift</th>
										<th>Tanggal</th>
										<th>Waktu</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ADDONS LIBRARY -->
<script src="<?php echo base_url('assets/modules/cleave-js/dist/cleave.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/modules/cleave-js/dist/addons/cleave-phone.au.js'); ?>"></script>

<script>
	var html5QrcodeScanner = new Html5QrcodeScanner(
		"reader", {
			fps: 10,
			qrbox: 250
		});

	function onScanSuccess(decodedText, decodedResult) {
		// Handle on success condition with the decoded text or result.
		//alert(decodedText+" _ "+ decodedResult);
		window.location.replace("<?php echo base_url("profile/do_lembur/masuk/"); ?>" + decodedText);
		// ...
		html5QrcodeScanner.clear();
		// ^ this will stop the scanner (video feed) and clear the scan area.
	}

	html5QrcodeScanner.render(onScanSuccess);


	const d = new Date();
	let months = d.getMonth() + 1;
	console.log(month + 1);
	conter = 1;
	table = $('#unit_tabel').DataTable({
		// Load data for the table's content from an Ajax source
		"ajax": {
			"url": '<?php echo site_url('profile/data_lembur/masuk/'); ?>' + months,
			"type": "POST"
		},
		"bFilter": false,
		//Set column definition initialisation properties.
		"columns": [{
				"data": null,
				"render": function(data, type, row) {
					return conter++;
				}
			},
			{
				"data": "nama_shift"
			},
			{
				"data": "tanggal"
			},
			{
				"data": "waktu"
			},
		],

	});

	$('#month').on('change', function() {


		table.ajax.url("<?php echo site_url('profile/data_lembur/masuk/'); ?>" + this.value).load();


	});
</script>