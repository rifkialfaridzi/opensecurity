<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        <br><img src="<?php echo base_url('assets/img/logo.png'); 
                            ?>" alt="logo" width="200"><br><br>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
        <a href="#">DHJ</a>
    </div>
    <ul class="sidebar-menu">
        <!--
        <li class="menu-header">Starter</li>
        
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Layout</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="layout-default.html">Default Layout</a></li>
                <li><a class="nav-link" href="layout-transparent.html">Transparent Sidebar</a></li>
                <li><a class="nav-link" href="layout-top-navigation.html">Top Navigation</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-ellipsis-h"></i> <span>Utilities</span></a>
            <ul class="dropdown-menu">
                <li><a href="utilities-contact.html">Contact</a></li>
                <li><a class="nav-link" href="utilities-invoice.html">Invoice</a></li>
                <li><a href="utilities-subscribe.html">Subscribe</a></li>
            </ul>
        </li>
-->
        <li <?php if ($this->uri->segment(1) == "admin") {
                echo 'class="active"';
            } ?>><a class="nav-link" href="<?php echo site_url("admin"); ?>"><i class="fas fa-fire"></i> <span>Dashboard</span></a></li>
        <li <?php if ($this->uri->segment(1) == "pegawai") {
                echo 'class="active"';
            } ?>><a class="nav-link" href="<?php echo site_url("pegawai"); ?>"><i class="fas fa-users"></i> <span>Pegawai</span></a></li>
        <li <?php if ($this->uri->segment(1) == "ijin") {
                echo 'class="active"';
            } ?>><a class="nav-link" href="<?php echo site_url("ijin/main"); ?>"><i class="fas fa-users"></i> <span>Perijinan</span></a></li>

        <li class="dropdown <?php if ($this->uri->segment(1) == "master") {
                                echo 'active';
                            } ?>">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Data Master</span></a>
            <ul class="dropdown-menu" <?php if ($this->uri->segment(1) == "master") {
                                            echo 'style="display: block;"';
                                        } ?>>
                <li <?php if ($this->uri->segment(2) == "jabatan") {
                        echo 'class="active"';
                    } ?>><a class="nav-link" href="<?php echo site_url("/master/jabatan"); ?>">Jabatan</a></li>
                <li <?php if ($this->uri->segment(2) == "shift") {
                        echo 'class="active"';
                    } ?>><a class="nav-link" href="<?php echo site_url("/master/shift"); ?>">Shift</a></li>
                <li <?php if ($this->uri->segment(2) == "lokasi") {
                        echo 'class="active"';
                    } ?>><a class="nav-link" href="<?php echo site_url("/master/lokasi"); ?>">Unit Kerja</a></li>
            </ul>
        </li>
    </ul>


</aside>