<section ng-app="myApp" ng-controller="someController" class="section">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="card card-statistic-2">
        <div class="card-chart">
          <canvas id="balance-chart" height="80"></canvas>
        </div>
        <div class="card-icon shadow-primary bg-primary">
          <i class="fas fa-users"></i>
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>Jumlah Pegawai</h4>
          </div>
          <div class="card-body">
            <?php echo $pegawai; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<h4>Statistik Akumulasi Seluruh Data</h4>
					</div>
					<div class="card-body">
						<canvas id="myChart" height="158"></canvas>
					</div>
				</div>
			</div>
		</div>
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4>Absen Terkini</h4>
        </div>
        <div class="card-body p-0">
          <div class="table-responsive table-invoice">
            <table class="table table-striped">
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Shift</th>
                <th>Waktu</th>
                <th>Action</th>
              </tr>
              <?php $i = 1;
              foreach ($recent_absen as $key) {

              ?>
                <tr>
                  <td> <?php echo $i; ?></td>
                  <td> <?php echo $key->nama_pegawai; ?></td>
                  <td> <?php echo $key->nama_shift; ?></td>
                  <td> <?php echo $key->waktu; ?></td>
                  <?php if ($key->jenis == "absen masuk") { ?>
                    <td> <a href="<?php echo site_url("aktivitas/detail/") . $key->id . "/" . $key->id_pegawai; ?>" class="btn btn-icon btn-primary"><i class="far fa-eye"></i></a>
                    </td>
                  <?php } else { ?>
                    <td><button disabled href="#" class="btn btn-icon btn-danger"><i class="far fa-eye"></i></button></td>
                  <?php } ?>
                </tr>

              <?php if (++$i > 4) break;
              } ?>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script>

var ctx = document.getElementById("myChart").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'doughnut',
		data: {
			labels: [
				'Tepat Waktu',
				'Terlambat',
				'Ijin'
			],
			datasets: [{
				label: 'My First Dataset',
				data: [<?php echo $jumlah_absen_tepat_waktu; ?>, <?php echo $jumlah_absen_terlambat; ?>, <?php echo $jumlah_ijin; ?>],
				backgroundColor: [
					'rgb(255, 99, 132)',
					'rgb(54, 162, 235)',
					'rgb(255, 205, 86)'
				],
				hoverOffset: 4
			}]
		},
	});
  
  var app = angular.module('myApp', ['ui.bootstrap']);

  app.controller('someController', function($scope, $filter, $http) {



    $scope.selectMonth = function(month) {
      $http.get('<?php echo base_url("admin/dashboard/"); ?>' + month, {
        msg: 'hello word!'
      }).
      then(function(response) {
        $scope.totalSupplier = response.data.supplier;
        $scope.totalOrder = response.data.totalOrder;
        $scope.totalProdukOrder = response.data.totalProdukOrder;
        $scope.totalPendapatan = response.data.totalPendapatan;
        $scope.topProduk = response.data.topProduk;
        $scope.totalProduk = response.data.produk.jumlah_produk;

        console.log(response.data);
      }, function(response) {
        console.log('error bos');
      });

    }

    var date = new Date();
    $scope.selectMonth(date.getMonth() + 1);





  });
</script>