<section class="section">
	<div class="section-header">
		<div class="section-header-back">
			<button onclick="history.back()" class="btn btn-icon"><i class="fas fa-arrow-left"></i></button>
		</div>
		<h1>Edit Pegawai</h1>
	</div>

	<div class="section-body">
		<?php if ($this->session->flashdata('pesan')) { ?>
			<div class="alert alert-warning alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					<?php echo $this->session->flashdata('pesan'); ?>
				</div>
			</div>
		<?php } ?>

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4>Edit Data Pegawai</h4>
					</div>
					<div class="card-body">
						<ul class="nav nav-tabs justify-content-center" id="myTab6" role="tablist">
							<li class="nav-item">
								<a class="nav-link active text-center" id="home-tab6" data-toggle="tab" href="#home6" role="tab" aria-controls="home" aria-selected="true">
									<span><i class="fas fa-user"></i></span> Profile</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-center" id="profile-tab6" data-toggle="tab" href="#profile6" role="tab" aria-controls="profile" aria-selected="false">
									<span><i class="fas fa-id-card"></i></span> Akun</a>
							</li>
						</ul>
						<div class="tab-content tab-bordered" id="myTabContent6">
							<div class="tab-pane fade show active" id="home6" role="tabpanel" aria-labelledby="home-tab6">
								<form method="POST" action="<?php echo base_url("pegawai/update_action/" . $data_pegawai[0]->user . "/profile"); ?>" enctype="multipart/form-data" class="needs-validation mt-4" novalidate="">
								<div class="form-group row mb-4">
										<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">NIP</label>
										<div class="col-sm-12 col-md-7">
											<input type="text" value="<?php echo $data_pegawai[0]->nik; ?>" class="form-control nik-formating" name="nik" required autofocus>
											<div class="invalid-feedback">
												NIP Masih Kosong
											</div>
										</div>
									</div>	
								
									<div class="form-group row mb-4">
										<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
										<div class="col-sm-12 col-md-7">
											<input id="name" value="<?php echo $data_pegawai[0]->nama; ?>" type="text" class="form-control" name="nama" tabindex="1" placeholder="Bambang" required autofocus>
											<div class="invalid-feedback">
												Nama Pegawai Masih Kosong
											</div>
										</div>
									</div>
									<div class="form-group row mb-4">
										<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tanggal Lahir</label>
										<div class="col-sm-12 col-md-7">
											<input id="tanggal_lahir" value="<?php echo $data_pegawai[0]->tanggal_lahir; ?>" type="text" class="form-control datepicker" name="tanggal_lahir" tabindex="1" required autofocus>
											<div class="invalid-feedback">
												Tanggal Lahir Masih Kosong
											</div>
										</div>
									</div>
									<div class="form-group row mb-4">
										<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nomor Telepon</label>
										<div class="col-sm-12 col-md-7">
											<input id="nik" value="<?php echo $data_pegawai[0]->nomor_telepon; ?>" type="text" class="form-control phone-numbers" name="nomor_telepon" required autofocus>
											<div class="invalid-feedback">
												Nomor Telepon Kosong
											</div>
										</div>
									</div>
									<div class="form-group row mb-4">
										<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Alamat</label>
										<div class="col-sm-12 col-md-7">
											<textarea name="alamat" class="summernote"><?php echo $data_pegawai[0]->alamat; ?></textarea>
											<div class="invalid-feedback">
												Alamat Masih Kosong
											</div>
										</div>
									</div>
									<div class="form-group row mb-4">
										<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tanggal Masuk</label>
										<div class="col-sm-12 col-md-7">
											<input id="tanggal_lahir" value="<?php echo $data_pegawai[0]->tanggal_masuk; ?>" type="text" class="form-control datepicker" name="tanggal_masuk" tabindex="1" required autofocus>
											<div class="invalid-feedback">
												Tanggal Masuk Masih Kosong
											</div>
										</div>
									</div>

									<div class="form-group row mb-4">
										<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jabatan</label>
										<div class="col-sm-12 col-md-7">
											<select name="jabatan" class="form-control selectric">
												<option value="">Pilih Jabatan</option>
												<?php foreach ($data_jabatan as $key) {  ?>
													<option <?php echo $data_pegawai[0]->jabatan == $key->nama ? "selected" : ""; ?> value="<?php echo $key->id; ?>"><?php echo $key->nama; ?></option>
												<?php  } ?>
											</select>
										</div>
									</div>
									<div class="form-group row mb-4">
										<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Unit Kerja</label>
										<div class="col-sm-12 col-md-7">
											<select name="lokasi" class="form-control selectric">
												<option value="">Pilih Unit Kerja</option>
												<?php foreach ($data_lokasi as $key) {  ?>
													<option <?php echo $data_pegawai[0]->lokasi == $key->id ? "selected" : ""; ?> value="<?php echo $key->id; ?>"><?php echo $key->nama; ?></option>
												<?php  } ?>
											</select>
											</select>
										</div>
									</div>
									<div class="form-group row mb-4">
										<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Shift</label>
										<div class="col-sm-12 col-md-7">
											<select name="shift" class="form-control selectric">
												<option value="">Pilih Shift</option>
												<?php foreach ($data_shift as $key) {  ?>
													<option <?php echo $data_pegawai[0]->shift == $key->id ? "selected" : ""; ?> value="<?php echo $key->id; ?>"><?php echo $key->nama; ?></option>
												<?php  } ?>
											</select>
											</select>
										</div>
									</div>
									<div class="form-group row mb-4">
										<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Foto</label>
										<div class="col-sm-12 col-md-7">
											<div id="image-preview" class="image-preview">
												<label for="image-upload" id="image-label">Pilih Gambar</label>
												<input type="file" name="foto" id="image-upload" accept="image/png, image/jpeg, image/jpg, image/gif" />
											</div>
											<div class="invalid-feedback">
												Nama Pegawai Masih Kosong
											</div>
										</div>
									</div>
									<div class="form-group row mb-4">
										<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
										<div class="col-sm-12 col-md-7">
											<button class="btn btn-primary">Ubah</button>
										</div>
									</div>
								</form>
							</div>
							<div class="tab-pane fade" id="profile6" role="tabpanel" aria-labelledby="profile-tab6">
								<form method="POST" action="<?php echo base_url("pegawai/update_action/" . $data_pegawai[0]->user . "/akun"); ?>" class="needs-validation mt-4" novalidate="">
									<div class="form-group row mb-4">
										<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Username</label>
										<div class="col-sm-12 col-md-7">
											<input id="name" type="text" value="<?php echo $data_pegawai[0]->username; ?>" class="form-control" name="username" tabindex="1" required autofocus>
											<div class="invalid-feedback">
												Username Pegawai Masih Kosong
											</div>
											<small class="text-muted">Atur Ulang Default Username</small>
										</div>
									</div>
									<div class="form-group row mb-4">
										<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Pasword</label>
										<div class="col-sm-12 col-md-7">
											<input id="name" type="password" class="form-control" name="password" tabindex="1" autofocus>
											<div class="invalid-feedback">
												Password Pegawai Masih Kosong
											</div>
											<small class="text-muted">Atur Ulang Default Password <span style="color:red;">(*Kosongkan Jika Tidak Mengganti Password)</span></small>
										</div>
									</div>
									<div class="form-group row mb-4">
										<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
										<div class="col-sm-12 col-md-7">
											<button class="btn btn-primary">Ubah</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ADDONS LIBRARY -->
<script src="<?php echo base_url('assets/modules/cleave-js/dist/cleave.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/modules/cleave-js/dist/addons/cleave-phone.au.js'); ?>"></script>

<script>
	var cleave = new Cleave('.phone-numbers', {
		numericOnly: true,
		delimiters: [' ', ' ', ' '],
		blocks: [4, 4, 4]
	});

	var cleave = new Cleave('.nik-formating', {
		numericOnly: true,
		delimiters: ['.', '.', '.', '.', '.', '-'],
		blocks: [2, 2, 2, 2, 2, 2, 4]
	});

	$.uploadPreview({
		input_field: "#image-upload", // Default: .image-upload
		preview_box: "#image-preview", // Default: .image-preview
		label_field: "#image-label", // Default: .image-label
		label_default: "Choose File", // Default: Choose File
		label_selected: "Change File", // Default: Change File
		no_label: false, // Default: false
		success_callback: null // Default: null
	});


	$("#image-preview").attr("style", "background-image:url('<?php echo site_url("assets/uploads/") . $data_pegawai[0]->foto; ?>'); background-size:cover; background-position:center center");
</script>