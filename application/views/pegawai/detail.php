<section class="section">
	<div class="section-header">
		<div class="section-header-back">
			<button onclick="history.back()" class="btn btn-icon"><i class="fas fa-arrow-left"></i></button>
		</div>
		<h1>Data Absen dan Perijinan</h1>
	</div>

	<div class="section-body">
		<?php if ($this->session->flashdata('pesan')) { ?>
			<div class="alert alert-warning alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					<?php echo $this->session->flashdata('pesan'); ?>
				</div>
			</div>
		<?php } ?>

		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<h4>Statistik Akumulasi Seluruh Data</h4>
					</div>
					<div class="card-body">
						<canvas id="myChart" height="158"></canvas>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4>Riwayat Absensi</h4>
						<div class="card-header-action">
							<form>
								<div class="input-group">
									<select id="month" name="month" class="form-control selectric">
										<option value="0">Pilih Periode&nbsp;</option>
										<option value="1">Januari &nbsp;&nbsp;&nbsp;</option>
										<option value="2">Februari</option>
										<option value="3">Maret</option>
										<option value="4">April</option>
										<option value="5">Mei</option>
										<option value="6">Juni</option>
										<option value="7">Juli</option>
										<option value="8">Agustus</option>
										<option value="9">September</option>
										<option value="10">Oktober</option>
										<option value="11">November</option>
										<option value="12">Desember</option>
									</select>
								</div>
							</form>

						</div>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<button disabled id="cetak_absensi" target="_blank" class="btn btn-icon icon-left btn-primary"><i class="fas fa-print"></i> Cetak</button>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="absensi_tabel" class="table table-striped">
								<thead>
									<tr>
										<th>No</th>
										<th>Shift</th>
										<th>Tanggal</th>
										<th>Waktu</th>
										<th>Jenis</th>
										<th>Lokasi</th>
										<th>Staus</th>
										<th>Detail</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4>Riwayat Ijin Dan Cuti</h4>
						<div class="card-header-action">
							<form>
								<div class="input-group">
									<select id="months" name="month" class="form-control selectric">
										<option value="0">Pilih Periode&nbsp;</option>
										<option value="1">Januari &nbsp;&nbsp;&nbsp;</option>
										<option value="2">Februari</option>
										<option value="3">Maret</option>
										<option value="4">April</option>
										<option value="5">Mei</option>
										<option value="6">Juni</option>
										<option value="7">Juli</option>
										<option value="8">Agustus</option>
										<option value="9">September</option>
										<option value="10">Oktober</option>
										<option value="11">November</option>
										<option value="12">Desember</option>
									</select>
								</div>
							</form>
						</div>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<button disabled id="cetak_ijin" target="_blank" class="btn btn-icon icon-left btn-primary"><i class="fas fa-print"></i> Cetak</button>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="unit_tabel" class="table table-striped">
								<thead>
									<tr>
										<th>No.</th>
										<th>Pegawai</th>
										<th>Jenis</th>
										<th>Jumlah Hari</th>
										<th>Keterangan</th>
										<th>Tanggal</th>
										<th>Status</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>

<!-- ADDONS LIBRARY -->
<script src="<?php echo base_url('assets/modules/cleave-js/dist/cleave.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/modules/cleave-js/dist/addons/cleave-phone.au.js'); ?>"></script>



<script>
	var ctx = document.getElementById("myChart").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'doughnut',
		data: {
			labels: [
				'Tepat Waktu',
				'Terlambat',
				'Ijin'
			],
			datasets: [{
				label: 'My First Dataset',
				data: [<?php echo $jumlah_absen_tepat_waktu; ?>, <?php echo $jumlah_absen_terlambat; ?>, <?php echo $jumlah_ijin; ?>],
				backgroundColor: [
					'rgb(255, 99, 132)',
					'rgb(54, 162, 235)',
					'rgb(255, 205, 86)'
				],
				hoverOffset: 4
			}]
		},
	});

	const d = new Date();
	let months = d.getMonth() + 1;
	conter = 1

	table = $('#absensi_tabel').DataTable({
		// Load data for the table's content from an Ajax source
		"ajax": {
			"url": '<?php echo site_url('pegawai/data_absen/') . $id_user . "/"; ?>' + months,
			"type": "POST"
		},
		"bFilter": false,
		//Set column definition initialisation properties.
		"columns": [{
				"data": null,
				"render": function(data, type, row) {
					return conter++;
				}
			},
			{
				"data": "nama_shift"
			},
			{
				"data": "tanggal"
			},
			{
				"data": "waktu"
			},
			{
				"data": "jenis"
			},
			{
				"data": null,
				"render": function(data, type, row) {
					return "Surakarta";
				}
			},
			{
				"data": null,
				"render": function(data, type, row) {
					return row.gap > 0 ? "Terlambat" : "Tepat Waktu";
				}
			},
			{
				"data": null,
				"render": function(data, type, row) {
					if (row.jenis == "absen masuk") {
						return '<a href="<?php echo site_url("aktivitas/detail/") ?>' + row.id + '/' + row.id_pegawai + '" class="btn btn-icon btn-primary"><i class="far fa-eye"></i></a>';
					} else {
						return '<button disabled href="#" class="btn btn-icon btn-danger"><i class="far fa-eye"></i></button>'
					}
				}
			}
		],

	});

	// $('#month').on('change', function() {


	// 	table.ajax.url("<?php //echo site_url('pegawai/data_absen/') . $id_user . "/"; 
							?>" + this.value).load();


	// });

	$(document).ready(function() {
		conters = 1
		tables1 = $('#unit_tabel').DataTable({
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": '<?php echo site_url('pegawai/data_ijin/') . $id_user . "/"; ?>' + months,
				"type": "POST"
			},
			"bFilter": false,
			//Set column definition initialisation properties.
			"columns": [{
					"data": null,
					"render": function(data, type, row) {
						return conters++;
					}
				},
				{
					"data": "nama_pegawai"
				},
				{
					"data": "jenis"
				},
				{
					"data": "jumlah_hari"
				},
				{
					"data": "keterangan"
				},
				{
					"data": "tanggal"
				},
				{
					"data": "status"
				},
			],

		});

		$('#months').on('change', function() {
			document.getElementById("cetak_ijin").disabled = false;
			tables1.ajax.url("<?php echo site_url('pegawai/data_ijin/') . $id_user . "/"; ?>" + this.value).load();
		});
		$('#month').on('change', function() {
			document.getElementById("cetak_absensi").disabled = false;
			table.ajax.url("<?php echo site_url('pegawai/data_absen/') . $id_user . "/"; ?>" + this.value).load();
		});

		$("#cetak_absensi").click(function() {
			window.open("<?php echo site_url('pegawai/print_absen/') . $id_user . "/" ?>" + $("#month").val());
			// console.log(getFirstDate);
			// console.log(getLastDate);
		});
		$("#cetak_ijin").click(function() {
			window.open("<?php echo site_url('pegawai/print_ijin/') . $id_user . "/" ?>" + $("#month").val());
			// console.log(getFirstDate);
			// console.log(getLastDate);
		});

	});
</script>