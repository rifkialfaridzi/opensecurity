<section class="section">
	<div class="section-header">
		<div class="section-header-back">
			<button onclick="history.back()" class="btn btn-icon"><i class="fas fa-arrow-left"></i></button>
		</div>
		<h1>Tambah Pegawai Baru</h1>
	</div>

	<div class="section-body">

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4>Buat Baru</h4>
					</div>
					<div class="card-body">

						<form method="POST" action="<?php echo base_url("pegawai/create_action"); ?>" enctype="multipart/form-data" class="needs-validation" novalidate="">
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">NIP</label>
								<div class="col-sm-12 col-md-7">
									<input type="text" class="form-control nik-formating" name="nik" required autofocus>
									<div class="invalid-feedback">
										NIP Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
								<div class="col-sm-12 col-md-7">
									<input id="name" type="text" class="form-control" name="nama" tabindex="1" placeholder="Bambang" required autofocus>
									<div class="invalid-feedback">
										Nama Pegawai Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tanggal Lahir</label>
								<div class="col-sm-12 col-md-7">
									<input id="tanggal_lahir" type="text" class="form-control datepicker" name="tanggal_lahir" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Tanggal Lahir Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nomor Telepon</label>
								<div class="col-sm-12 col-md-7">
									<input id="nik" type="text" class="form-control phone-numbers" name="nomor_telepon" required autofocus>
									<div class="invalid-feedback">
										Nomor Telepon Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Alamat</label>
								<div class="col-sm-12 col-md-7">
									<textarea name="alamat" class="summernote"></textarea>
									<div class="invalid-feedback">
										Alamat Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tanggal Masuk</label>
								<div class="col-sm-12 col-md-7">
									<input id="tanggal_lahir" type="text" class="form-control datepicker" name="tanggal_masuk" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Tanggal Masuk Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jabatan</label>
								<div class="col-sm-12 col-md-7">
									<select name="jabatan" class="form-control selectric" required autofocus>
										<option value="">Pilih Jabatan</option>
										<?php foreach ($jabatan as $key) {  ?>
											<option value="<?php echo $key->id; ?>"><?php echo $key->nama; ?></option>
										<?php  } ?>
									</select>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Unit Kerja</label>
								<div class="col-sm-12 col-md-7">
									<select name="lokasi" class="form-control selectric" required autofocus>
										<option value="">Pilih Unit Kerja</option>
										<?php foreach ($lokasi as $key) {  ?>
											<option value="<?php echo $key->id; ?>"><?php echo $key->nama; ?></option>
										<?php  } ?>
									</select>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Shift</label>
								<div class="col-sm-12 col-md-7">
									<select name="shift" class="form-control selectric" required autofocus>
										<option value="">Pilih Shift</option>
										<?php foreach ($shift as $key) {  ?>
											<option value="<?php echo $key->id; ?>"><?php echo $key->nama; ?></option>
										<?php  } ?>
									</select>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Foto</label>
								<div class="col-sm-12 col-md-7">
									<div id="image-preview" class="image-preview">
										<label for="image-upload" id="image-label">Pilih Gambar</label>
										<input type="file" name="foto" id="image-upload" accept="image/png, image/jpeg, image/jpg, image/gif" />
									</div>
									<div class="invalid-feedback">
										Nama Pegawai Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
								<div class="col-sm-12 col-md-7">
									<button class="btn btn-primary">Tambahkan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ADDONS LIBRARY -->
<script src="<?php echo base_url('assets/modules/cleave-js/dist/cleave.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/modules/cleave-js/dist/addons/cleave-phone.au.js'); ?>"></script>



<script>
	var cleave = new Cleave('.phone-numbers', {
		numericOnly: true,
		delimiters: [' ', ' ', ' '],
		blocks: [4, 4, 4]
	});

	var cleave = new Cleave('.nik-formating', {
		numericOnly: true,
		delimiters: ['.', '.', '.', '.', '.', '-'],
		blocks: [2, 2, 2, 2, 2, 2, 4]
	});

	$.uploadPreview({
		input_field: "#image-upload", // Default: .image-upload
		preview_box: "#image-preview", // Default: .image-preview
		label_field: "#image-label", // Default: .image-label
		label_default: "Choose File", // Default: Choose File
		label_selected: "Change File", // Default: Change File
		no_label: false, // Default: false
		success_callback: null // Default: null
	});
</script>