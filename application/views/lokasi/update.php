<section class="section">
	<div class="section-header">
		<div class="section-header-back">
			<button onclick="history.back()" class="btn btn-icon"><i class="fas fa-arrow-left"></i></button>
		</div>
		<h1>Ubah Lokasi</h1>
	</div>

	<div class="section-body">

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4>Ubah Lokasi : <?php echo $data_Lokasi->nama;?></h4>
					</div>
					<div class="card-body">

						<form method="POST" action="<?php echo base_url("Lokasi/update_action/".$data_Lokasi->id); ?>" class="needs-validation" novalidate="">
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Lokasi</label>
								<div class="col-sm-12 col-md-7">
									<input id="nama" type="text" class="form-control" name="nama" value="<?php echo $data_Lokasi->nama;?>" tabindex="1" placeholder="<?php echo $data_Lokasi->nama;?>" required autofocus>
									<div class="invalid-feedback">
										Nama Lokasi Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Alamat</label>
								<div class="col-sm-12 col-md-7">
									<input type="text" class="form-control" name="alamat" tabindex="1" value="<?php echo $data_Lokasi->alamat;?>" required autofocus>
									<div class="invalid-feedback">
										Alamat Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Masa Kontrak</label>
								<div class="col-sm-12 col-md-7">
								<input id="tanggal_lahir" type="text" class="form-control datepicker" name="masa_kontrak" value="<?php echo $data_Lokasi->masa_kontrak;?>" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Masa Kontrak Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
								<div class="col-sm-12 col-md-7">
									<button class="btn btn-primary">Ubah</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>