<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laporan Ijin Secrity</title>
</head>
<body>
<table cellpadding="1" cellspacing="0" style="border-collapse:collapse;margin:auto; padding:10px; width:90%">

<tbody>
    <tr>
        <td style="text-align:left;width:20%">
            <img style="width: auto;height:150px" src="<?php echo base_url('assets/img/logo.png') ?>"></img>
        </td>
        <td style="text-align:center">
            <h1 style="text-align:center;padding:0;margin:0"><strong>PT. INVESTAMA KOMANDO SECURITY</strong></h1>
            <small>Dusun II, Makamhaji, Kartasura, Sukoharjo 57161</small>
            <h4>Laporan Ijin Pegawai Periode <?php echo date("F", mktime(0, 0, 0, $periode, 10)) ?></h4>
        </td>
    </tr>
</tbody>

</table>

<hr>

<div style="text-align:center">

<p>&nbsp;</p>
<h4 style="text-align: left;margin-bottom: -0.3em;margin-left: 0.5em;">A. Data Pegawai</h4>
<table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:100%">
    <tbody>
        <tr>

            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Nama<b</td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Jabatan<b</td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Unit Kerja<b</td>
        </tr>

        <?php //foreach ($data as $key) { 
        ?>
        <tr>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $pegawai[0]->nama; ?></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $pegawai[0]->jabatan; ?></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $pegawai[0]->nama_lokasi; ?></td>
        </tr>
        <?php //} 
        ?>
    </tbody>
</table>

<h4 style="text-align: left;margin-bottom: -0.3em;margin-left: 0.5em;">B. Riwayat Ijin</h4>
<table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:100%">
    <tbody>
        <tr>

            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Nama</b></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Jenis</b></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Jumlah Hari</b></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>keterangan</b></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Tanggal</b></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Status</b></td>
        </tr>

        <?php foreach ($data_ijin as $key) { 
        ?>
        <tr>
        <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->nama_pegawai; ?></td>
        <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->jenis; ?></td>
        <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->jumlah_hari; ?></td>
        <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->keterangan; ?></td>
        <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->tanggal; ?></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->status; ?></td>
           
        </tr>
        <?php } 
        ?>
    </tbody>
</table>

<p>&nbsp;</p>

<p>&nbsp;</p>

<table cellpadding="1" cellspacing="0" style="page-break-inside: avoid;text-align:center;border-collapse:collapse; border:none; margin:auto; padding:10px; width:100%">
    <tbody>
        <tr>
            <td>Yang Mengetahui,</td>
        </tr>
        <tr>
            <!-- <td><span style="font-size:16px"><strong>Badaruddin</strong></span></td> -->
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>PT Investama Komando Security</td>
        </tr>
    </tbody>
</table>
</div>
</body>
</html>