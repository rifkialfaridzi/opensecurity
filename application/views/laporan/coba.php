<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ID CARD <?php echo $data[0]->nama; ?></title>
    <style>
        .card{
            width: 5.5cm;
            height:9cm;
            border: 1px solid black;
            border-radius:10px;
            padding: 1em;
            text-align: center;
        }
    </style>
</head>

<body>
    <div class="card">
        <img style="width: auto;height:52px" src="<?php echo base_url('assets/img/logo.png') ?>"></img>
        <hr>
        <h5 style="margin: 0;">PT INVESTAMA KOMANDO SECURITY</h5>
        <img style="width: auto;height:120px;border-radius:50%;margin-top:0.6em;margin-bottom:0.6em" src="<?php echo base_url('assets/uploads/').$data[0]->foto; ?>"></img>
        <h4 style="margin: 0;"><?php echo $data[0]->nama; ?></h4>
       
        <!-- <img style="width: auto;height:65px;margin-top:0.8em" src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=<?php //echo base_url()."?page=". //md5($dataPegawai[0]->user); ?>&choe=UTF-8"></img> -->

        <img style="width: auto;height:65px;margin-top:0.8em" src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=<?php echo md5($data[0]->user); ?>&choe=UTF-8"></img>
        
    </div>
</body>

</html>