<section class="section">
	<div class="section-header">
		<h1>Halaman Perijinan</h1>
	</div>

	<div class="section-body">

		<?php if ($this->session->flashdata('pesan')) { ?>
			<div class="alert alert-warning alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					<?php echo $this->session->flashdata('pesan'); ?>
				</div>
			</div>
		<?php } ?>

		<div class="row">
			<div class="col-lg-12 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Data Perijinan</h4>
                        <div class="card-header-action">
							<a href="<?php echo base_url('/ijin/create'); ?>" class="btn btn-primary">
							<i class="fa fa-plus"></i> Tambah Perijinan
							</a>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="tabel_ijin" class="table table-striped">
								<thead>
									<tr>
										<th>No.</th>
										<th>Pegawai</th>
										<th>Jenis</th>
										<th>Jumlah Hari</th>
										<th>Keterangan</th>
										<th>Tanggal</th>
										<th>Status</th>
										<th>Catatan</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalDelete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Hapus Perijinan</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Apakah Anda Yakin ?</p>
			</div>
			<div class="modal-footer bg-whitesmoke br">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a id="btn-delete" type="button" href="#" class="btn btn-danger">Hapus</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var save_method; //for save method string
	var table;

	$(document).ready(function() {
		//datatables
		counter = 1;
		table = $('#tabel_ijin').DataTable({
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": '<?php echo site_url('ijin/json'); ?>',
				"type": "POST"
			},
			//Set column definition initialisation properties.
			"columns": [{
					"data": null,
					"render": function(data, type, row) {
						return counter++;
					}
				},
				{
					"data": "nama_pegawai"
				},
				{
					"data": "jenis"
				},
				{
					"data": "jumlah_hari"
				},
				{
					"data": "keterangan"
				},
				{
					"data": "tanggal"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						
						if (row.status == 'diproses') {
							return "<div class='badge badge-info'>Diproses</div>";
						}else if(row.status == 'diterima'){
							return "<div class='badge badge-success'>Diterima</div>";
						}else{
							return "<div class='badge badge-danger'>Ditolak</div>"
						}
						
					}
					
				},
				{
					"data": null,
					"render": function(data, type, row) {
						
						if (row.status_keterangan == "") {
							return "tidak ada catatan";
						}else{
							return row.status_keterangan;
						}
						
					}
					
				},
				{
					"data": null,
					"render": function(data, type, row) {
						return '<a href="<?php echo site_url("ijin/edit/") ?>' + row.id + '" class="btn btn-icon btn-primary"><i class="far fa-edit"></i></a> <a onclick=deleteConfirm("' + row.id + '") href="#!" class="btn btn-icon btn-danger exampleModalDelete" data-toggle="modal" data-target="#exampleModalDelete"><i class="fas fa-times"></i></a>';
					}
				}
			],

		});



	});

	function deleteConfirm(url) {
		$('#btn-delete').attr('href', "<?php echo site_url("ijin/delete"); ?>/" + url);
		$('#exampleModalDelete').modal();
	}
</script>