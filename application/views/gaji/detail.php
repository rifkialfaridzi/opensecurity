<section class="section">
	<div class="section-header">
		<h1>Halaman Detail Periode Gaji</h1>
	</div>

	<div class="section-body">

		<?php if ($this->session->flashdata('pesan')) { ?>
			<div class="alert alert-warning alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					<?php echo $this->session->flashdata('pesan'); ?>
				</div>
			</div>
		<?php } ?>

		<div class="row">
			<div class="col-lg-12 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Data Detail Periode Gaji</h4>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="tabel_ijin" class="table table-striped">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama</th>
										<th>Jabatan</th>
										<th>Jumlah Masuk</th>
										<th>Jumlah Cuti</th>
										<th>Jumlah Ijin</th>
										<th>Tidak Masuk</th>
										<th>Jumlah Lembur</th>
										<th>Gaji</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $counter = 1; foreach ($data_gaji as $key) { //var_dump($key['jumlah_hari_kerja']);?>
									<tr>
										<td><?php echo $counter++; ?></td>
										<td><?php echo $key['nama']; ?></td>
										<td><?php echo $key['jabatan']; ?></td>
										<td><?php echo $key['jumlah_hari_kerja']; ?></td>
										<td><?php echo $key['jumlah_cuti']; ?></td>
										<td><?php echo $key['jumlah_ijin']; ?></td>
										<td><?php echo $key['jumlah_hari_tidakmasuk']; ?></td>
										<td><?php echo $key['jumlah_jam_lembur'] . " Jam"; ?></td>
										<td><?php echo "Rp ". number_format($key['total_gaji']); ?></td>
										<td><a href="<?php echo site_url("gaji/pegawai/").$key['id_gaji']."/".$key['id_user']; ?>" class="btn btn-icon btn-success"><i class="fas fa-info-circle"></i></a></td>
									</tr>
									<?php }?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalDelete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Hapus Perijinan</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Apakah Anda Yakin ?</p>
			</div>
			<div class="modal-footer bg-whitesmoke br">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a id="btn-delete" type="button" href="#" class="btn btn-danger">Hapus</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function deleteConfirm(url) {
		$('#btn-delete').attr('href', "<?php echo site_url("gaji/delete"); ?>/" + url);
		$('#exampleModalDelete').modal();
	}
	
</script>