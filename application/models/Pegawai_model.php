<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pegawai_model extends CI_Model
{

    public $table = 'pegawai';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {
        $this->db->select('p.*,j.nama as jabatan,l.nama as lokasi');
        $this->db->from('pegawai p');
        $this->db->join('jabatan j', 'p.jabatan=j.id', 'left');
        $this->db->join('lokasi l', 'p.lokasi=l.id', 'left');
        return $this->db->get()->result();
    }

    function get_by_id_all($id) {
        $this->db->select('p.*,j.nama as jabatan,l.nama as lokasi');
        $this->db->from('pegawai p');
        $this->db->join('shift s', 'p.shift=s.id', 'left');
        $this->db->join('jabatan j', 'p.jabatan=j.id', 'left');
        $this->db->join('lokasi l', 'p.lokasi=l.id', 'left');
        return $this->db->where('p.user', $id)->get();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    function get_all_Pegawai()
    {
        $this->db->where("level", 3);
        $this->db->order_by($this->id, $this->order);
        return $this->db->get("user")->result();
    }

    // Insert Data 

    function insert_user($data)
    {
        $this->db->trans_start();
        $this->db->insert("user", $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
    }
    function insert_pegawai($data)
    {
        $this->db->insert("pegawai", $data);
    }


    function record_absen_user_all(){
        
        $this->db->select('rb.*, s.nama as nama_shift, p.user as id_pegawai');
        $this->db->from('record_absen rb'); 
        $this->db->join('shift s', 's.id=rb.shift', 'left');
        $this->db->join('pegawai p', 'p.user=rb.pegawai', 'left');
        $this->db->where('rb.jenis',"absen masuk");
        return$this->db->order_by('rb.tanggal',"DESC")->get()->result();
         
    }
    function record_ijin_user_all() {
        $this->db->select('i.*, p.nama as nama_pegawai');
        $this->db->from('ijin i'); 
        $this->db->join('pegawai p', 'p.user=i.pegawai', 'left');
        $this->db->where('i.status',"diterima");
       return $this->db->order_by('i.tanggal DESC, i.jenis ASC')->get()->result();
    }


    function record_absen_by_user_all($id){
        
        $this->db->select('rb.*, s.nama as nama_shift, p.user as id_pegawai');
        $this->db->from('record_absen rb'); 
        $this->db->join('shift s', 's.id=rb.shift', 'left');
        $this->db->join('pegawai p', 'p.user=rb.pegawai', 'left');
        $this->db->where('rb.jenis',"absen masuk");
        $this->db->order_by('rb.tanggal',"DESC");
        return $this->db->where('rb.pegawai',$id)->get()->result();
    }
    function record_ijin_by_user_all($id) {
        $this->db->select('i.*, p.nama as nama_pegawai');
        $this->db->from('ijin i'); 
        $this->db->join('pegawai p', 'p.user=i.pegawai', 'left');
        $this->db->where('i.pegawai',$id);
        $this->db->where('i.status',"diterima");
       return $this->db->order_by('i.tanggal DESC, i.jenis ASC')->get()->result();
    }



    function record_absen_by_user($id, $month){
        
        $this->db->select('rb.*, s.nama as nama_shift, p.user as id_pegawai');
        $this->db->from('record_absen rb'); 
        $this->db->join('shift s', 's.id=rb.shift', 'left');
        $this->db->join('pegawai p', 'p.user=rb.pegawai', 'left');
        $this->db->where('MONTH(rb.tanggal)',$month);
        $this->db->order_by('rb.tanggal',"DESC");
        return $this->db->where('rb.pegawai',$id)->get()->result();
    }

    function record_ijin_by_user($id,$month) {
        $this->db->select('i.*, p.nama as nama_pegawai');
        $this->db->from('ijin i'); 
        $this->db->join('pegawai p', 'p.user=i.pegawai', 'left');
        $this->db->where('i.pegawai',$id);
        $this->db->where('MONTH(i.tanggal)',$month);
       return $this->db->order_by('i.tanggal DESC, i.jenis ASC')->get()->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->select('p.*,j.nama as jabatan,u.username as username, s.nama as nama_shift, s.id as id_shift, s.jam_masuk as jam_masuk, s.jam_pulang as jam_pulang, l.nama as nama_lokasi');
        $this->db->from('pegawai p');
        $this->db->join('jabatan j', 'p.jabatan=j.id', 'left');
        $this->db->join('user u', 'p.user=u.id', 'left');
        $this->db->join('shift s', 'p.shift=s.id', 'left');
        $this->db->join('lokasi l', 'p.lokasi=l.id', 'left');
        $this->db->where("p.user", $id);
        return $this->db->get()->result();
    }

    function get_pegawai()
    {
        $this->db->select('p.*,j.nama as jabatan,u.username as username');
        $this->db->from('pegawai p');
        $this->db->join('jabatan j', 'p.jabatan=j.id', 'left');
        $this->db->join('user u', 'p.user=u.id', 'left');
        return $this->db->get()->result();
    }
    

    function get_Pegawai_by_id($id)
    {
        $this->db->where("user", $id);
        return $this->db->get("pegawai")->row();
    }

    // get data by id
    function get_by_idPegawai($idPegawai)
    {
        $this->db->select('p.*,j.nama as nama_jabatan, l.nama as unit_kerja');
        $this->db->from('user u');
        $this->db->join('pegawai p', 'p.user=u.id', 'right');
        $this->db->join('jabatan j', 'j.id=p.jabatan', 'right');
        $this->db->join('lokasi l', 'l.id=p.lokasi', 'right');
        return $this->db->where('p.user', $idPegawai)->get();
        
    }

    // get data by posisi
    function get_by_posisi($posisi)
    {
        $this->db->select('p.*,u.name,u.id as id_pegawai, u.nomor_punggung, u.posisi');
        $this->db->from('user u');
        $this->db->join('Pegawai p', 'p.user=u.id', 'right');
        return $this->db->where('u.posisi', $posisi)->get();
        
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
	$this->db->or_like('nama', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
	$this->db->or_like('nama', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where("user", $id);
        $this->db->update($this->table, $data);
    }
    function update_profile($id, $data)
    {
        $this->db->where("id", $id);
        $this->db->update("user", $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where("user", $id);
        $this->db->delete($this->table);
    }
    function delete_user($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete("user");
    }

}

/* End of file Category_model.php */
/* Location: ./application/models/Category_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */